<?php
/**
 * 表单提交的结果以及验证码是否正确
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/25
 * Time: 17:37
 */

header('content-type:text/html;charset=utf-8');
session_start();
echo '用户名为：'. $_POST['username'].'<br>';
echo '密码为：'. $_POST['password'].'<br>';
echo '输入的验证码为：'. $_POST['verify'].'<br>';
echo '正确的验证码为：'. $_SESSION['verifyCode'].'<br>';
echo '<hr>';
if ($_POST['verify'] === strtolower($_SESSION['verifyCode'])) {
    echo '验证码正确';
} else {
    echo '验证码错误';
}
