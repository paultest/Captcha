<?php
require_once 'extend/Captcha.php';

$test = new Captcha(array('fontfile' => 'fonts/simkai_new.ttf', 'type' => 4, 'pixel' => 100, 'line' => 3, 'arc' => 2));

session_start();
$_SESSION['verifyCode'] = $test->getCaptcha();
